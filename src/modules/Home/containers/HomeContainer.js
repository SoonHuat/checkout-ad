import React from 'react'
import CheckoutContainer from '../../Checkout/containers/CheckoutContainer'

const HomeContainer = (props) => (
  <div>
    <CheckoutContainer />
  </div>
)

export default HomeContainer
