import { connect } from 'react-redux'
import CheckoutComponent from '../components/CheckoutComponent'
import { calculatePrice } from '../CheckoutActions'

const mapStateToProps = (state) => ({
  price: state.checkout.price
})

const mapDispatchToProps = (dispatch) => ({
  calculatePrice: (customer, skus) => {
    dispatch(calculatePrice(customer, skus))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutComponent)
