const initialState = {
  isCalculating: false,
  errorMessage: null,
  price: 0
}

export default (state = initialState, action) => {
  switch (action.type) {
    case 'CALCULATING_PRICE':
      return Object.assign({}, state, {
        isCalculating: true
      })
    case 'SUCCESS_CALCULATE_PRICE':
      return Object.assign({}, state, {
        isCalculating: false,
        price: action.price,
        errorMessage: null
      })
    case 'FAIL_CALCULATE_PRICE':
      return Object.assign({}, state, {
        isCalculating: false,
        errorMessage: action.errorMessage
      })
    default:
      return state
  }
}
