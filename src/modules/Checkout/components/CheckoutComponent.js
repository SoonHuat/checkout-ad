import React, { Component } from 'react'
import { Debounce } from 'react-throttle'
import Paper from 'material-ui/Paper'
import TextField from 'material-ui/TextField'
import { MaterailStyle } from './CheckoutJsStyle'

require('./Checkout.scss')

class CheckoutComponent extends Component {

  constructor (props) {
    super(props)
    this.state = {
      price: 0,
      customer: '',
      skus: []
    }
    this.handleCustomerInput = this.handleCustomerInput.bind(this)
    this.handleDeleteSku = this.handleDeleteSku.bind(this)
    this.handleSkuAdd = this.handleSkuAdd.bind(this)
    this.calculatePrice = this.calculatePrice.bind(this)
  }

  componentWillMount () {
  }

  componentWillReceiveProps (nextProps) {
    console.log(nextProps.price)
    this.setState({
      price: nextProps.price
    })
  }

  handleCustomerInput (customer) {
    this.setState({
      customer: customer.target.value
    })
  }

  handleSkuAdd (event) {
    event.preventDefault()
    let sku = this.refs.skuInputRef.getValue()
    this.setState({
      skus: this.state.skus.slice().concat([sku])
    })
    this.refs.skuInputRef.input.value = ''
  }

  handleDeleteSku (item) {
    let newSkus = this.state.skus
    let index = item.target.value
    if (newSkus.length >= index) {
      newSkus.splice(index, 1)
      this.setState({
        skus: newSkus
      })
    }
  }

  calculatePrice () {
    this.props.calculatePrice(this.state.customer, this.state.skus)
  }

  render () {
    return (
      <div className='container'>
        <Paper zDepth={1} className='container-paper'>
          <div className='col col-12'>
            <h1>Checkout</h1>
            <div className='col col-6 checkout-field'>
              <div className='col col-12'>
                <Debounce time='500' handler='onChange'>
                  <TextField
                    floatingLabelStyle={MaterailStyle.themeColor}
                    underlineStyle={MaterailStyle.themeBorderColor}
                    underlineFocusStyle={MaterailStyle.themeBorderColor}         
                    hintText='Customer name'
                    floatingLabelText='Customer'
                    floatingLabelFixed={true}
                    onChange={this.handleCustomerInput}
                  />
                </Debounce>
              </div>
              <div className='col col-12 button-section'>
                <button className='btn btn-green' onClick={this.calculatePrice}>Calculate</button>
              </div>
              <div className='col col-12'>
                <label>Total Price: ${this.state.price}</label>
              </div>
            </div>
            <div className='col col-6 checkout-field'>
              <div className='col col-12'>
                <TextField
                  ref='skuInputRef'
                  floatingLabelStyle={MaterailStyle.themeColor}
                  underlineStyle={MaterailStyle.themeBorderColor}
                  underlineFocusStyle={MaterailStyle.themeBorderColor}                
                  hintText='SKU ID'
                  floatingLabelText='SKU'
                  floatingLabelFixed={true}
                  />
                <button className='btn btn-green btn-add' onClick={this.handleSkuAdd}>Add</button>
              </div>
              <div className='col col-12'>
                <label>SKU: </label>
              </div>
              {
                this.state.skus.map((sku, i) => (
                  <div className='col col-12 sku-item'>
                    <label className='col col-10' key={i}>{sku}</label>
                    <button onClick={this.handleDeleteSku} className='col col-2 btn-delete' value={i}>Remove</button>
                  </div>
                ))
              }
            </div>
            <div className='clearfix'></div>
          </div>
        </Paper>
      </div>
    )
  }
}
export default CheckoutComponent
