import axios from 'axios'
import _ from 'lodash'

import { CalculatePremiumAdPrice } from './GenericCalculator'

const API_URL = 'http://localhost:8000/api/'

export const calculatePrice = (customer = '', SKUs = []) => async (dispatch) => {
  dispatch(_calculating())
  let price = 0
  let advertisements
  try {
    advertisements = await _getAdvertisement()
    let consolidatedSku = _consolidateSKU(SKUs)
    let formattedCustomer = _.toUpper(customer)
    price = CalculatePremiumAdPrice(formattedCustomer, consolidatedSku, advertisements)
  } catch (e) {
    return dispatch(_failedCalculate(e))
  }
  return dispatch(_successCalculate(price))
}

const _getAdvertisement = async () => {
  let response
  try {
    response = await axios.get(`${API_URL}advertisements`)
  } catch (e) {
    console.log(e)
  }
  return response.data.advertisement
}

const _consolidateSKU = (SKUs) => {
  let consolidatedSku = {}
  SKUs.forEach(function (sku) {
    let formattedSku = _.toLower(sku.trim())
    if (consolidatedSku[formattedSku]) {
      consolidatedSku[formattedSku]++
    }
    else {
      consolidatedSku[formattedSku] = 1
    }
  }, this)
  return consolidatedSku
}

const _calculating = () => ({
  type: 'CALCULATING_PRICE'
})

const _successCalculate = (price) => ({
  type: 'SUCCESS_CALCULATE_PRICE',
  price
})

const _failedCalculate = (errorMessage) => ({
  type: 'FAIL_CALCULATE_PRICE',
  errorMessage
})
