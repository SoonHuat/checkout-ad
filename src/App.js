import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import getMuiTheme from 'material-ui/styles/getMuiTheme'

import AppContainer from './modules/App/containers/AppContainer'
import runtime from 'serviceworker-webpack-plugin/lib/runtime'
import registerEvents from 'serviceworker-webpack-plugin/lib/browser/registerEvents'
import * as OfflinePluginRuntime from 'offline-plugin/runtime'
import 'bootstrap'
require('font-awesome/scss/font-awesome.scss')

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: '#ff1919',
    accent1Color: '#2675ff',
    shadowColor: '#2675ff',
    borderColor: '#f1f1f1'
  },
  checkbox: {
    boxColor: '#c1c1c1'
  },
  radioButton: {
    borderColor: '#c1c1c1'
  },
  button: {
    height: 39
  },
  textField: {
    borderColor: '#f1f1f1',
    floatingLabelFocusColor: '#c1c1c1',
    floatingLabelColor: '#c1c1c1'
  },
  fontFamily: 'Open Sans, sans-serif',
  appBar: {
    height: 50,
  },
})

class MainApp extends React.Component {
  componentDidUpdate (prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0)
    }
  }

  componentDidMount () {
    if ('serviceWorker' in navigator && window.location.protocol === 'https:' && !process.env.DISABLE_SERVICE_WORKER) {
      OfflinePluginRuntime.install()
      const registration = runtime.register()
      registerEvents(registration, {
        onInstalled () {
          console.log('[ServiceWorker] service worker was installed')
        },
        onUpdateReady () {
          console.log('[ServiceWorker] service worker update is ready')
        },

        onUpdating () {
          console.log('[ServiceWorker] service worker is updating')
        },

        onUpdateFailed () {
          console.log('[ServiceWorker] service worker update failed')
        },

        onUpdated () {
          console.log('[ServiceWorker] service worker was updated')
        }
      })
    }
  }

  render () {
    return (
      <Provider store={this.props.store}>
        <MuiThemeProvider muiTheme={muiTheme} >
          <BrowserRouter>
            <Switch>
              <Route path='/' component={AppContainer} />
            </Switch>
          </BrowserRouter>
        </MuiThemeProvider>
      </Provider>
    )
  }
}

export default MainApp
